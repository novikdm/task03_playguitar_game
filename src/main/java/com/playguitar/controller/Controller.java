package com.playguitar.controller;

import com.playguitar.model.action.SongAction;
import com.playguitar.model.action.Sound;
import com.playguitar.model.creator.SongCreator;
import com.playguitar.model.dao.SongDAO;
import com.playguitar.model.entity.Song;
import com.playguitar.model.exceptions.MelodyLogicalException;

public class Controller {

    private static final SongCreator SONG_CREATOR = new SongCreator();
    private static final SongAction SONG_ACTION = SongAction.getInstance();

    public void playSound(String path) {
        Sound.playSound(path).play();
    }

    public String[] getSongFromDB(String name){
        SongDAO dao = new SongDAO();
        Song song = dao.findSongByName(name);
        return SONG_ACTION.unpackSounds(song.getSounds());
    }

    public void playSong(String name) {
        Song played = null;
        try {
            played = new Song(SONG_ACTION.createSounds(getSongFromDB(name)[0],
                     getSongFromDB(name)[1]), name);
        } catch (MelodyLogicalException e) {
            e.printStackTrace();
        }
        SONG_ACTION.playSong(played);
    }

    public String[] getSong(String name) {
        return SONG_ACTION.unpackSounds(SONG_CREATOR.createSong(name).getSounds());
    }
}
