package com.playguitar.view;

import java.awt.event.KeyEvent;
import java.io.*;

import com.playguitar.controller.Controller;
import com.playguitar.view.ui.ConsoleHandler;
import com.playguitar.view.ui.MyConsole;
import com.playguitar.view.ui.menu.FirstMenu;

import static com.playguitar.view.ConstantsForGuitar.*;

public class Menu {
    private String pathToResources = "E:\\EPAM\\final\\src\\main\\resources";
    private static final Controller CONTROLLER = new Controller();

    private void clearScreen() throws IOException, InterruptedException {
        new ProcessBuilder("cmd", "/c", "cls").inheritIO().start().waitFor();
    }

    public void showMenu() throws IOException {
        outTxtFile(pathToResources + "\\Menu.txt");
    }

    private void outTxtFile(String path) throws IOException {
        InputStream input = new BufferedInputStream(new FileInputStream(path));
        byte[] buffer = new byte[8192];

        try {
            for (int length = 0; (length = input.read(buffer)) != -1;) {
                System.out.write(buffer, 0, length);
            }
        } finally {
            input.close();
            System.out.println();
        }
    }

    private void outStaticTop() throws IOException {
        outTxtFile(pathToResources + "\\StaticTop.txt");
    }

    private void outStaticBottom() throws IOException {
        outTxtFile(pathToResources + "\\StaticBottom.txt");
    }

    public void outGuitar(KeyEvent event) {
    KeyEvent sound = event;
    try {
        clearScreen();
        outChangedGuitar(getFret(sound));
        Thread.sleep(150);
        String path = pathToResources + "\\sounds\\" + getFret(sound) + ".wav";
        CONTROLLER.playSound(path);
        clearScreen();
    } catch (InterruptedException | IOException e) {
        e.printStackTrace();
    }
    outSimpleGuitar();
}

    private void outSimpleGuitar() {
        try {
            outStaticTop();
            outNonStaticStrings();
            outStaticBottom();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void outChangedGuitar(String numOfGuitarString) {
        try {
            outStaticTop();
            outNonStaticStrings(numOfGuitarString);
            outStaticBottom();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void outNonStaticStrings() {
        System.out.println(getClearString(0));
        System.out.println(getClearString(1));
        System.out.println(getClearString(2));
        System.out.println(getClearString(3));
    }

    private void outNonStaticStrings(String numOfGuitarString) {
        int numOfFret = Integer.parseInt(String.valueOf(numOfGuitarString.charAt(1)));
        int numOfString = Integer.parseInt(String.valueOf(numOfGuitarString.charAt(0)));
        if(numOfString == 0) {
            System.out.println(getNeededString(numOfFret, numOfString));
            System.out.println(getClearString(1));
            System.out.println(getClearString(2));
            System.out.println(getClearString(3));
        }
        if(numOfString == 1) {
            System.out.println(getClearString(0));
            System.out.println(getNeededString(numOfFret, numOfString));
            System.out.println(getClearString(2));
            System.out.println(getClearString(3));
        }
        if(numOfString == 2) {
            System.out.println(getClearString(0));
            System.out.println(getClearString(1));
            System.out.println(getNeededString(numOfFret, numOfString));
            System.out.println(getClearString(3));
        }
        if(numOfString == 3) {
            System.out.println(getClearString(0));
            System.out.println(getClearString(1));
            System.out.println(getClearString(2));
            System.out.println(getNeededString(numOfFret, numOfString));
        }
    }

    private String getClearString(int numOfString) {
        String clearString = null;
        if(numOfString == 0) {
            clearString = START_OF_FIRST_STRING + generateString('.')
                    + STATIC_PART_OF_FIRST + DYNAMIC_PART_OF_FIRST;
        }
        if(numOfString == 1) {
            clearString = START_OF_SECOND_STRING + generateString('|')
                    + STATIC_PART_OF_OTHER + DYNAMIC_PART_OF_SECOND;
        }
        if(numOfString == 2) {
            clearString = START_OF_THIRD_STRING + generateString('|')
                    + STATIC_PART_OF_OTHER + DYNAMIC_PART_OF_THIRD;
        }
        if(numOfString == 3) {
            clearString = START_OF_FOURTH_STRING + generateString('|')
                    + STATIC_PART_OF_OTHER + DYNAMIC_PART_OF_FOURTH;
        }
        return clearString;
    }

    private String getNeededString(int numOfFret, int numOfString) {
        String guitarString = null;
            if (numOfString == 0) {
                guitarString = START_OF_FIRST_STRING ;
                if (numOfFret == 0) {
                    guitarString += generateString('.');
                    guitarString += STATIC_PART_OF_FIRST;
                    guitarString += CHANGED_PART_OF_FIRST;
                } else {
                    guitarString += generateString(numOfFret, '.');
                    guitarString += STATIC_PART_OF_FIRST;
                    guitarString += DYNAMIC_PART_OF_FIRST;
                }
                return guitarString;
            }
            if (numOfString == 1) {
                guitarString = START_OF_SECOND_STRING;
                if (numOfFret == 0) {
                    guitarString += generateString('|');
                    guitarString += STATIC_PART_OF_OTHER;
                    guitarString += CHANGED_PART_OF_SECOND;
                } else {
                    guitarString += generateString(numOfFret, '|');
                    guitarString += STATIC_PART_OF_OTHER;
                    guitarString += DYNAMIC_PART_OF_SECOND;
                }
                return guitarString;
            }
            if (numOfString == 2) {
                guitarString = START_OF_THIRD_STRING;
                if (numOfFret == 0) {
                    guitarString += generateString('|');
                    guitarString += STATIC_PART_OF_OTHER;
                    guitarString += CHANGED_PART_OF_THIRD;
                } else {
                    guitarString += generateString(numOfFret, '|');
                    guitarString += STATIC_PART_OF_OTHER;
                    guitarString += DYNAMIC_PART_OF_THIRD;
                }
                return guitarString;
            }
            if (numOfString == 3) {
                guitarString = START_OF_FOURTH_STRING;
                if (numOfFret == 0) {
                    guitarString += generateString('|');
                    guitarString += STATIC_PART_OF_OTHER;
                    guitarString += CHANGED_PART_OF_FOURTH;
                } else {
                    guitarString += generateString(numOfFret, '|');
                    guitarString += STATIC_PART_OF_OTHER;
                    guitarString += DYNAMIC_PART_OF_FOURTH;
                }
                return guitarString;
            }
            return guitarString;
        }

    private String generateString(int numOfFret, char separator) {
    String string = "";
    int startNumburingOfStrings = 10;//numbering of strings starts from the end of griff
        for (int i = 1; i < 11; i++) {
            if (i == startNumburingOfStrings  - numOfFret) {
                string += "********" + separator;
                continue;
            }
            string += "________" + separator;
        }
        return string;
    }

    private String generateString(char separator) {
        String string = "";
        for (int i = 0; i < 10; i++) {
            string += "________" + separator;
        }
        return string;
    }

    private String getFret(KeyEvent keyEvent) {
            if (keyEvent.getKeyCode() == KeyEvent.VK_1) {
                return "09";
            } else if (keyEvent.getKeyCode() == KeyEvent.VK_2) {
                return "08";
            } else if (keyEvent.getKeyCode() == KeyEvent.VK_3) {
                return "07";
            } else if (keyEvent.getKeyCode() == KeyEvent.VK_4) {
                return "06";
            } else if (keyEvent.getKeyCode() == KeyEvent.VK_5) {
                return "05";
            } else if (keyEvent.getKeyCode() == KeyEvent.VK_6) {
                return "04";
            } else if (keyEvent.getKeyCode() == KeyEvent.VK_7) {
                return "03";
            } else if (keyEvent.getKeyCode() == KeyEvent.VK_8) {
                return "02";
            } else if (keyEvent.getKeyCode() == KeyEvent.VK_9) {
                return "01";
            } else if (keyEvent.getKeyCode() == KeyEvent.VK_0) {
                return "00";
            } else if (keyEvent.getKeyCode() == KeyEvent.VK_Q) {
                return "19";
            } else if (keyEvent.getKeyCode() == KeyEvent.VK_W) {
                return "18";
            } else if (keyEvent.getKeyCode() == KeyEvent.VK_E) {
                return "17";
            } else if (keyEvent.getKeyCode() == KeyEvent.VK_R) {
                return "16";
            } else if (keyEvent.getKeyCode() == KeyEvent.VK_T) {
                return "15";
            } else if (keyEvent.getKeyCode() == KeyEvent.VK_Y) {
                return "14";
            } else if (keyEvent.getKeyCode() == KeyEvent.VK_U) {
                return "13";
            } else if (keyEvent.getKeyCode() == KeyEvent.VK_I) {
                return "12";
            } else if (keyEvent.getKeyCode() == KeyEvent.VK_O) {
                return "11";
            } else if (keyEvent.getKeyCode() == KeyEvent.VK_P) {
                return "10";
            } else if (keyEvent.getKeyCode() == KeyEvent.VK_A) {
                return "29";
            } else if (keyEvent.getKeyCode() == KeyEvent.VK_S) {
                return "28";
            } else if (keyEvent.getKeyCode() == KeyEvent.VK_D) {
                return "27";
            } else if (keyEvent.getKeyCode() == KeyEvent.VK_F) {
                return "26";
            } else if (keyEvent.getKeyCode() == KeyEvent.VK_G) {
                return "25";
            } else if (keyEvent.getKeyCode() == KeyEvent.VK_H) {
                return "24";
            } else if (keyEvent.getKeyCode() == KeyEvent.VK_J) {
                return "23";
            } else if (keyEvent.getKeyCode() == KeyEvent.VK_K) {
                return "22";
            } else if (keyEvent.getKeyCode() == KeyEvent.VK_L) {
                return "21";
            } else if (keyEvent.getKeyCode() == KeyEvent.VK_SEMICOLON) {
                return "20";
            } else if (keyEvent.getKeyCode() == KeyEvent.VK_Z) {
                return "39";
            } else if (keyEvent.getKeyCode() == KeyEvent.VK_X) {
                return "38";
            } else if (keyEvent.getKeyCode() == KeyEvent.VK_C) {
                return "37";
            } else if (keyEvent.getKeyCode() == KeyEvent.VK_V) {
                return "36";
            } else if (keyEvent.getKeyCode() == KeyEvent.VK_B) {
                return "35";
            } else if (keyEvent.getKeyCode() == KeyEvent.VK_N) {
                return "34";
            } else if (keyEvent.getKeyCode() == KeyEvent.VK_M) {
                return "33";
            } else if (keyEvent.getKeyCode() == KeyEvent.VK_COMMA) {
                return "32";
            } else if (keyEvent.getKeyCode() == KeyEvent.VK_PERIOD) {
                return "31";
            } else if (keyEvent.getKeyCode() == KeyEvent.VK_SLASH) {
                return "30";
            } else {
                return "Bad";
            }
        }

    public void show() {
        MyConsole console = new MyConsole();
        console.setVisible(true);
        ConsoleHandler menuController = new ConsoleHandler(console, this);
        FirstMenu firstMenu = new FirstMenu(console);
        firstMenu.initializeEntries();
        firstMenu.printMenu();
        console.setKeyHandler(menuController.getKeyHandler());
    }

    public void playAllGuitar() {
        String soundsDuration[] = CONTROLLER.getSong("Brigada");
        String allSounds[] = soundsDuration[0].split(" ");
        String dur[] = soundsDuration[1].split(" ");
        float durations[] = new float[dur.length];
        for(int i = 0; i < dur.length; i++) {
            durations[i] = Float.parseFloat(dur[i]);
        }
        for(int i = 0; i < dur.length; i ++) {
            try {
                clearScreen();
                outChangedGuitar(allSounds[i]);
                String path = pathToResources + "\\sounds\\" + allSounds[i] + ".wav";
                CONTROLLER.playSound(path);
                Thread.sleep((long)(durations[i] * 400));
                clearScreen();
            } catch (InterruptedException | IOException e) {
                e.printStackTrace();
            }
            outSimpleGuitar();
        }


    }
    //зробити так, щоб пысля цього методу викликався outGuitar

    public int tryRepeat(int end) {
        String soundsDuration[] = CONTROLLER.getSong("Piraty Carybskogo Morya");
        String allSounds[] = soundsDuration[0].split(" ");
        String dur[] = soundsDuration[1].split(" ");
        float durations[] = new float[dur.length];
        for(int i = 0; i < dur.length; i++) {
            durations[i] = Float.parseFloat(dur[i]);
        }
        for(int i = 0; i < end; i ++) {
                try {
                    clearScreen();
                    outChangedGuitar(allSounds[i]);
                    String path = pathToResources + "\\sounds\\" + allSounds[i] + ".wav";
                    CONTROLLER.playSound(path);
                    Thread.sleep((long) (durations[i] * 400));
                    clearScreen();
                } catch (InterruptedException | IOException e) {
                    e.printStackTrace();
                }
                outSimpleGuitar();
        }
        return dur.length;
    }
}
