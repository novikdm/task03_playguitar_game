package com.playguitar.view.ui;

import com.playguitar.view.Menu;
import com.playguitar.view.ui.menu.*;

import java.awt.event.KeyEvent;
import java.io.IOException;


public class ConsoleHandler {
    private MyConsole myConsole;
    private Menu myMenu;
    private int counter = 2;
    private int counterForGuitar = 2;
    private int maxSounds = 100;
    private FirstMenuEntry selectedEntry;
    private static int count = 0;

    public ConsoleHandler(MyConsole console, Menu menu) {
        myConsole = console;
        myMenu = menu;
    }

    public int getCounter() {
        return counter;
    }

    public void setCounter(int counter) {
        this.counter = counter;
    }

    private void start() {
        myConsole.clear();
        selectedEntry = new StartEntry();
        selectedEntry.run(myConsole);
    }

    private void startRepeatMode() {
        myConsole.clear();
        selectedEntry = new RepeatEntry();
        selectedEntry.run(myConsole);
    }

    private void startAll() {
        myConsole.clear();
        selectedEntry = new AllEntry();
        selectedEntry.run(myConsole);
    }

    private void exit() {
        selectedEntry = new ExitEntry();
        selectedEntry.run(myConsole);
    }

    public int getMaxSounds() {
        return maxSounds;
    }

    public void setMaxSounds(int maxSounds) {
        this.maxSounds = maxSounds;
    }

    public KeyHandler getKeyHandler() {
        return new KeyHandler() {
            @Override
            public void keyPressed(KeyEvent e) {
                if (count == 0) {
                    if (e.getKeyCode() == KeyEvent.VK_1) {
                        count++;
                        System.out.println("Start play");
                        start();
                    } else if (e.getKeyCode() == KeyEvent.VK_2) {
                        count++;
                        System.out.println("Start repeat");
                        startRepeatMode();
                    } else if (e.getKeyCode() == KeyEvent.VK_3) {
                        count++;
                        System.out.println("Play all");
                        startAll();
                    } else if (e.getKeyCode() == KeyEvent.VK_4) {
                        count++;
                        System.out.println("Come back, Jack");
                        exit();
                    }
                    myConsole.clear();
                } else if ((e.getKeyCode() == KeyEvent.VK_0) || (e.getKeyCode() == KeyEvent.VK_1)
                        || (e.getKeyCode() == KeyEvent.VK_2) || (e.getKeyCode() == KeyEvent.VK_3)
                        || (e.getKeyCode() == KeyEvent.VK_4) || (e.getKeyCode() == KeyEvent.VK_5)
                        || (e.getKeyCode() == KeyEvent.VK_6) || (e.getKeyCode() == KeyEvent.VK_7)
                        || (e.getKeyCode() == KeyEvent.VK_8) || (e.getKeyCode() == KeyEvent.VK_9)
                        || (e.getKeyCode() == KeyEvent.VK_Q) || (e.getKeyCode() == KeyEvent.VK_W)
                        || (e.getKeyCode() == KeyEvent.VK_E) || (e.getKeyCode() == KeyEvent.VK_R)
                        || (e.getKeyCode() == KeyEvent.VK_T) || (e.getKeyCode() == KeyEvent.VK_Y)
                        || (e.getKeyCode() == KeyEvent.VK_U) || (e.getKeyCode() == KeyEvent.VK_I)
                        || (e.getKeyCode() == KeyEvent.VK_O) || (e.getKeyCode() == KeyEvent.VK_P)
                        || (e.getKeyCode() == KeyEvent.VK_A) || (e.getKeyCode() == KeyEvent.VK_S)
                        || (e.getKeyCode() == KeyEvent.VK_D) || (e.getKeyCode() == KeyEvent.VK_F)
                        || (e.getKeyCode() == KeyEvent.VK_G) || (e.getKeyCode() == KeyEvent.VK_H)
                        || (e.getKeyCode() == KeyEvent.VK_J) || (e.getKeyCode() == KeyEvent.VK_K)
                        || (e.getKeyCode() == KeyEvent.VK_L) || (e.getKeyCode() == KeyEvent.VK_SEMICOLON)
                        || (e.getKeyCode() == KeyEvent.VK_Z) || (e.getKeyCode() == KeyEvent.VK_X)
                        || (e.getKeyCode() == KeyEvent.VK_C) || (e.getKeyCode() == KeyEvent.VK_V)
                        || (e.getKeyCode() == KeyEvent.VK_B) || (e.getKeyCode() == KeyEvent.VK_N)
                        || (e.getKeyCode() == KeyEvent.VK_M) || (e.getKeyCode() == KeyEvent.VK_COMMA)
                        || (e.getKeyCode() == KeyEvent.VK_PERIOD) || (e.getKeyCode() == KeyEvent.VK_SLASH)) {
                    myMenu.outGuitar(e);
                } else {

                }
            }

            @Override
            public void keyReleased(KeyEvent e) {

            }
        };
    }

    public KeyHandler getKeyHandlerForRepetition() {
        return new KeyHandler() {
            @Override
            public void keyPressed(KeyEvent e) {
                try {
                    if ((e.getKeyCode() == KeyEvent.VK_0) || (e.getKeyCode() == KeyEvent.VK_1)
                            || (e.getKeyCode() == KeyEvent.VK_2) || (e.getKeyCode() == KeyEvent.VK_3)
                            || (e.getKeyCode() == KeyEvent.VK_4) || (e.getKeyCode() == KeyEvent.VK_5)
                            || (e.getKeyCode() == KeyEvent.VK_6) || (e.getKeyCode() == KeyEvent.VK_7)
                            || (e.getKeyCode() == KeyEvent.VK_8) || (e.getKeyCode() == KeyEvent.VK_9)
                            || (e.getKeyCode() == KeyEvent.VK_Q) || (e.getKeyCode() == KeyEvent.VK_W)
                            || (e.getKeyCode() == KeyEvent.VK_E) || (e.getKeyCode() == KeyEvent.VK_R)
                            || (e.getKeyCode() == KeyEvent.VK_T) || (e.getKeyCode() == KeyEvent.VK_Y)
                            || (e.getKeyCode() == KeyEvent.VK_U) || (e.getKeyCode() == KeyEvent.VK_I)
                            || (e.getKeyCode() == KeyEvent.VK_O) || (e.getKeyCode() == KeyEvent.VK_P)
                            || (e.getKeyCode() == KeyEvent.VK_A) || (e.getKeyCode() == KeyEvent.VK_S)
                            || (e.getKeyCode() == KeyEvent.VK_D) || (e.getKeyCode() == KeyEvent.VK_F)
                            || (e.getKeyCode() == KeyEvent.VK_G) || (e.getKeyCode() == KeyEvent.VK_H)
                            || (e.getKeyCode() == KeyEvent.VK_J) || (e.getKeyCode() == KeyEvent.VK_K)
                            || (e.getKeyCode() == KeyEvent.VK_L) || (e.getKeyCode() == KeyEvent.VK_SEMICOLON)
                            || (e.getKeyCode() == KeyEvent.VK_Z) || (e.getKeyCode() == KeyEvent.VK_X)
                            || (e.getKeyCode() == KeyEvent.VK_C) || (e.getKeyCode() == KeyEvent.VK_V)
                            || (e.getKeyCode() == KeyEvent.VK_B) || (e.getKeyCode() == KeyEvent.VK_N)
                            || (e.getKeyCode() == KeyEvent.VK_M) || (e.getKeyCode() == KeyEvent.VK_COMMA)
                            || (e.getKeyCode() == KeyEvent.VK_PERIOD) || (e.getKeyCode() == KeyEvent.VK_SLASH)) {
                        if (maxSounds == counterForGuitar + 1) {
                            myMenu.showMenu();
                        }
                        myMenu.outGuitar(e);
                        if (counter == counterForGuitar) {
                            Thread.sleep(500);
                            myMenu.tryRepeat(counterForGuitar);
                            counterForGuitar++;
                            counter = 1;
                        }
                        counter++;
                    } else {
                        if (maxSounds == counterForGuitar + 1) {
                            myMenu.showMenu();
                        }
                        myMenu.outGuitar(e);
                        if (counter == counterForGuitar) {
                            Thread.sleep(500);
                            myMenu.tryRepeat(counterForGuitar);
                            counterForGuitar++;
                            counter = 1;
                        }
                        counter++;
                    }
                } catch (InterruptedException e1) {
                    e1.printStackTrace();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }

            @Override
            public void keyReleased(KeyEvent e) {

            }
        };
    }

}