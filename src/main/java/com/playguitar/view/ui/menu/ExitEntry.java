package com.playguitar.view.ui.menu;

import com.playguitar.view.ui.MyConsole;

public class ExitEntry extends FirstMenuEntry {
    public ExitEntry() {
        this.setMenuEntry("Exit the program");
    }

    @Override
    public void run(MyConsole myConsole) {
        myConsole.setText("Come back, Jack");
        Runtime.getRuntime().exit(0);
    }
}
