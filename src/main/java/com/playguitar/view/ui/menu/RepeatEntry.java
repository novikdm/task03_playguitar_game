package com.playguitar.view.ui.menu;

import com.playguitar.view.Menu;
import com.playguitar.view.ui.ConsoleHandler;
import com.playguitar.view.ui.MyConsole;

public class RepeatEntry extends FirstMenuEntry {
    private String option;

    public RepeatEntry() {
        option = "Start the repeat mode";
        this.setMenuEntry(option);
    }

    @Override
    public void run(MyConsole myConsole) {
        Menu fMenu = new Menu();
        ConsoleHandler menuController = new ConsoleHandler(myConsole, fMenu);
        menuController.setMaxSounds(fMenu.tryRepeat(1));
        myConsole.setKeyHandler(menuController.getKeyHandlerForRepetition());
    }
}
