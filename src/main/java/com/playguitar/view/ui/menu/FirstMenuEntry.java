package com.playguitar.view.ui.menu;

import com.playguitar.view.ui.MyConsole;

public abstract class FirstMenuEntry {
    private String menuEntry;

    public abstract void run(MyConsole myConsole);

    final String getMenuEntry() {
        return menuEntry;
    }

    final void setMenuEntry(final String entry) {
        menuEntry = entry;
    }
}

