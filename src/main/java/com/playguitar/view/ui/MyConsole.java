package com.playguitar.view.ui;

import javax.swing.*;
import java.awt.*;

public class MyConsole extends JFrame {
    private MyTextArea myTextArea = null;
    private KeyHandler keyHandler = null;

    public MyConsole() {
        super("Guitar Hero");
        setSize(new Dimension(300, 300));
        setLocationRelativeTo(null);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setResizable(true);

        myTextArea = new MyTextArea();
        getContentPane().add(myTextArea);
    }

    public void setKeyHandler(KeyHandler handler) {
        myTextArea.removeKeyListener(keyHandler);
        keyHandler = handler;
        myTextArea.addKeyListener(keyHandler);
    }

    public void setText(String string) {
        myTextArea.setText(string + "\n");
    }

    public void clear() {
        myTextArea.clear();
        this.setSize(new Dimension(1,1));
        this.setResizable(false);
    }

    final class MyTextArea extends JTextArea {

        MyTextArea() {
            super();
            setBackground(Color.BLACK);
            setForeground(Color.WHITE);
            setEditable(false);
            setFont(Font.decode("Consolas").deriveFont(18f));
        }

        public void clear() {
            super.setText("");
            super.updateUI();
        }
    }
}