package com.playguitar.model.exceptions;

public class SongLogicalException extends Exception {
    public SongLogicalException() {
    }

    public SongLogicalException(String s) {
        super(s);
    }

    public SongLogicalException(String s, Throwable throwable) {
        super(s, throwable);
    }

    public SongLogicalException(Throwable throwable) {
        super(throwable);
    }

    public SongLogicalException(String s, Throwable throwable, boolean b, boolean b1) {
        super(s, throwable, b, b1);
    }
}
