package com.playguitar.model;

import com.playguitar.model.action.Sound;

import java.io.File;

public class PlaySound {
  public void play(String soundName){
    Sound.playSound(System.getProperty("user.dir") + File.separator
            + "src"
            + File.separator
            + "main"
            + File.separator
            + "java"
            + File.separator
            + "com" + File.separator
            + "playguitar" + File.separator
            + "resources" + File.separator
            + "sounds" + File.separator
            + soundName +
            ".wav"
    ).join();
  }

}
