package com.playguitar.model.action;

import com.playguitar.model.entity.Melody;
import com.playguitar.model.entity.Song;
import com.playguitar.model.exceptions.MelodyLogicalException;

public class SongAction {
    private final static SongAction INSTANCE = new SongAction();
    private SongAction() {

    }

    public static SongAction getInstance() {
        return INSTANCE;
    }

    public Melody[] createSounds(String audio, String duration) throws MelodyLogicalException {
        String[] manyAudio = audio.split(" ");
        String[] manyDurations = duration.split(" ");
        Melody[] sounds = new Melody[manyAudio.length];
        for(int i = 0; i < manyAudio.length; i ++) {
            sounds[i] = new Melody(manyAudio[i], Float.parseFloat(manyDurations[i]));
        }
        return sounds;
    }

    public String[] unpackSounds(Melody[] sounds) {
        String audio = "";
        String durations = "";
        for (Melody sound : sounds) {
            audio += sound.getName() + " ";
            durations += sound.getDuration()+ " ";
        }
        return new String[]{audio,durations};
    }

    public void playSong(Song song) {
        String path = "E:\\EPAM\\final\\src\\main\\resources\\sounds\\";
        String[] audioDuration = unpackSounds(song.getSounds());
        String audio[] = audioDuration[0].split(" ");
        String duration[] = audioDuration[1].split(" ");
        int musicLengthOfNotes = audio.length;
        float[] dur = new float[musicLengthOfNotes];
        for (int i = 0; i < musicLengthOfNotes; i++) {
            dur[i] = Float.parseFloat(duration[i]);
        }
        Sound.playSound(path + audio[0] + ".wav").play();
        for (int i = 0; i <  musicLengthOfNotes; i++) {
            Sound.playSound(path + audio[i] + ".wav").play();
            try {
                Thread.sleep((long)((dur[i] * 400)));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}

