package com.playguitar.model.dao;

import com.playguitar.model.action.SongAction;
import com.playguitar.model.action.WrapperConnector;
import com.playguitar.model.entity.Song;
import com.playguitar.model.exceptions.MelodyLogicalException;
import com.playguitar.model.exceptions.SongLogicalException;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class SongDAO extends AbstractDAO<Integer, Song> {
    private static final String SQL_SELECT_ALL_SONGS = "SELECT * FROM song";
    private static final String SQL_SELECT_SONG_BY_ID = "SELECT * FROM song WHERE Song_ID = ?";
    private static final String SQL_DELETE_SONG_BY_ID = "DELETE FROM song WHERE Song_ID = ?";
    private static final String SQL_DELETE_SONG_BY_ALL = "DELETE FROM song WHERE Name = ? AND "
            + "Sounds = ? AND Duration = ?";
    private static final String SQL_CREATE_SONG = "INSERT INTO Song (Song_ID, Name, Sounds, " +
            "Duration) VALUES (?, ?, ?, ?)";
    private static final String SQL_UPDATE_SONG_BY_ID = "UPDATE song SET Name = ?, " +
            "Sounds = ?, Duration = ? WHERE Song_ID = ?";
    private static final String SQL_SELECT_SONG_BY_NAME = "SELECT * FROM song WHERE Name = ?";
    private static final SongAction SONG_ACTION = SongAction.getInstance();

    public SongDAO() {
        this.connector = new WrapperConnector();
    }

    @Override
    public List<Song> findAll() {
        List<Song> songs = new ArrayList<>();
        Statement st = null;
        try {
            st = connector.getStatement();
            ResultSet resultSet = st.executeQuery(SQL_SELECT_ALL_SONGS);
            while (resultSet.next()) {
                Song song = new Song();
                song.setName(resultSet.getString("Name"));
                song.setId(resultSet.getInt("Song_ID"));
                song.setSounds(SONG_ACTION.createSounds(resultSet.getString("Sounds"),
                        resultSet.getString("Duration")));
                songs.add(song);
            }
        } catch (SQLException e) {
            System.err.println("SQL exception (request or table failed): " + e);
        } catch (SongLogicalException e) {
            System.err.println("Bad id for Song");
        } catch (MelodyLogicalException e) {
            System.err.println("Not existing Melody");
        } finally {
            this.closeStatement(st);
        }
        return songs;
    }

    @Override
    public Song findSongById(Integer id) {
        PreparedStatement st = null;
        Song song = new Song();
        try {
            st = connector.getPreparedStatement(SQL_SELECT_SONG_BY_ID);
            st.setInt(1,id);
            setAll(song, st);
        } catch (SQLException e) {
            System.err.println("SQL exception (request or table failed): " + e);
        } catch (SongLogicalException e) {
            System.err.println("Bad id for Song");
        } catch (MelodyLogicalException e) {
            System.err.println("Not existing melody");
        } finally {
            this.closeStatement(st);
        }
        return song;
    }

    @Override
    public boolean delete(Song song) {
        PreparedStatement st = null;
        boolean done = false;
        String audioAndDurations[] = SONG_ACTION.unpackSounds(song.getSounds());
        try {
            st = connector.getPreparedStatement(SQL_DELETE_SONG_BY_ALL);
            setNameSoundsDuration(st,song,audioAndDurations);
            if (st.executeUpdate() > 0) {
                done = true;
            }
        } catch (SQLException e) {
            System.err.println("SQL exception (request or table failed): " + e);
        } finally {
            this.closeStatement(st);
        }
        return done;
    }

    @Override
    public boolean create(Song song) {
        PreparedStatement st = null;
        boolean done = false;
        String audioAndDurations[] = SONG_ACTION.unpackSounds(song.getSounds());
        try {
            st = connector.getPreparedStatement(SQL_CREATE_SONG);
            st.setInt(1, song.getId());
            st.setString(2, song.getName());
            st.setString(3, audioAndDurations[0]);
            st.setString(4, audioAndDurations[1]);
            if (st.executeUpdate() > 0) {
                done = true;
            }
        } catch (SQLException e) {
            System.err.println("SQL exception (request or table failed): " + e);
        } finally {
            this.closeStatement(st);
        }
        return done;
    }

    @Override
    public boolean delete(Integer id) {
        PreparedStatement st = null;
        boolean done = false;
        try {
            st = connector.getPreparedStatement(SQL_DELETE_SONG_BY_ID);
            st.setInt(1,id);
            if (st.executeUpdate() > 0) {
                done = true;
            }
        } catch (SQLException e) {
            System.err.println("SQL exception (request or table failed): " + e);
        } finally {
            this.closeStatement(st);
        }
        return done;
    }

    private void setNameSoundsDuration(PreparedStatement st, Song song,
                                       String audioAndDurations[]) throws SQLException {
        st.setString(1, song.getName());
        st.setString(2, audioAndDurations[0]);
        st.setString(3, audioAndDurations[1]);
    }

    @Override
    public Song update(Song song) {
        PreparedStatement st = null;
        String audioAndDurations[] = SONG_ACTION.unpackSounds(song.getSounds());
        try {
            st = connector.getPreparedStatement(SQL_UPDATE_SONG_BY_ID);
            setNameSoundsDuration(st,song,audioAndDurations);
            st.setInt(4,song.getId());
            st.executeUpdate();
        } catch (SQLException e) {
            System.err.println("SQL exception (request or table failed): " + e);
        } finally {
            this.closeStatement(st);
        }
        return song;
    }

    @Override
    public Song findSongByName(String name) {
        PreparedStatement st = null;
        Song song = new Song();
        try {
            st = connector.getPreparedStatement(SQL_SELECT_SONG_BY_NAME);
            st.setString(1,name);
            setAll(song, st);
        } catch (SQLException e) {
            System.err.println("SQL exception (request or table failed): " + e);
        } catch (SongLogicalException e) {
            System.err.println("Bad id for Song");
        } catch (MelodyLogicalException e) {
            System.err.println("Not existing melody");
        } finally {
            this.closeStatement(st);
        }
        return song;
    }

    private void setAll(Song song, PreparedStatement st) throws SQLException,
            MelodyLogicalException, SongLogicalException {
        ResultSet resultSet = st.executeQuery();
        resultSet.next();
        song.setName(resultSet.getString("Name"));
        song.setId(resultSet.getInt("Song_ID"));
        song.setSounds(SONG_ACTION.createSounds(resultSet.getString("Sounds"),
                resultSet.getString("Duration")));
    }
}
