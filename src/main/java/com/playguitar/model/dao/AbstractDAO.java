package com.playguitar.model.dao;

import com.playguitar.model.action.WrapperConnector;
import com.playguitar.model.entity.Song;

import java.sql.Statement;
import java.util.List;

public abstract class AbstractDAO<K,T extends Song> implements AutoCloseable{
    protected WrapperConnector connector;

    public abstract List<T> findAll();
    public abstract T findSongById(K id);
    public abstract T findSongByName(String name);
    public abstract boolean delete(K id);
    public abstract boolean delete(T entity);
    public abstract boolean create(T entity);
    public abstract T update(T entity);

    @Override
    public void close() {
        connector.closeConnection();
    }
    
    protected void closeStatement(Statement statement) {
        connector.closeStatement(statement);
    }

}
